(function(){
    const btns = document.querySelectorAll(".company__circle-main");
    const clsBtns = document.querySelectorAll(".company__cls-btn");
    let zIdx = 0;

    btns.forEach(e => e.addEventListener('click', function(){
        zIdx++;
        this.nextElementSibling.style.opacity = "1";
        this.nextElementSibling.style.zIndex = zIdx.toString();
        this.nextElementSibling.classList.add('active');
        console.log(this.nextElementSibling.style.zIndex);
    }));

    clsBtns.forEach(e => e.addEventListener('click', function(){
        if(window.innerWidth > 768){
            zIdx--;
            this.parentElement.parentElement.style.opacity = "0";
            this.parentElement.parentElement.style.zIndex = "-1";
            this.parentElement.parentElement.classList.remove('active');
        }
    }));
})();

(function(){
    const modals = document.querySelectorAll('.company__modal');
    window.addEventListener('resize', setZIndex);

})();

function setZIndex(){
    const modals = document.querySelectorAll('.company__modal');
    if(window.innerWidth < 768){
        modals.forEach(e => {
            e.style.opacity = "1";
            e.style.zIndex = "0";
        });
    }else{
        modals.forEach(e => {
            e.classList.remove('active');
            e.style.opacity = "0";
            e.style.zIndex = "-1";
        });
    }
}
