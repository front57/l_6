$(document).ready(function(){
    $('.hero__slider').slick({
        prevArrow: $(".hero__prev"),
        nextArrow: $(".hero__next"),
        slidesToShow:1,
        dots:true,
        customPaging(slider, i){
            return "<a class='hero__dots'>0" + (i + 1) + "</a>";
        },
        appendDots: $('.hero__dots'),

    });

    $('.resp__slider').slick({
        prevArrow: $(".resp__prev"),
        nextArrow: $(".resp__next"),
        slidesToShow:1,
        slidesToScroll:1,
        infinite:true,
        variableWidth: true,
        variableHeight:true,
        responsive: [
            {
                breakpoint:450,
                settings:{
                    //variableWidth: false,
                    centerMode: true,
                    //infinite: true,

                    centerPadding: "15px",
                }
            }
        ]
    });
});



