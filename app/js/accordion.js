const btns = document.querySelectorAll(".accordion__btn, .footer__title-btn");
const company = document.querySelectorAll('.company__m-header');

company.forEach(e => e.addEventListener('click', function(){
    if (window.innerWidth < 768){
        this.classList.toggle('active');
        const cardBody = this.nextElementSibling;
        if (cardBody.style.maxHeight){
            cardBody.style.maxHeight = null;
        }else{
            cardBody.style.maxHeight = cardBody.scrollHeight + "px";
        }
    }
}));

btns.forEach(e => e.addEventListener('click', function(){
    if (window.innerWidth < 450){
        this.classList.toggle('active');
        const panel = this.nextElementSibling;
        panel.classList.toggle('active');
        if (panel.style.maxHeight){
            panel.style.maxHeight = null;
        }else{
            panel.style.maxHeight = panel.scrollHeight + 'px';
        }
    }
}));

window.addEventListener('resize', function(){
    if (window.innerWidth > 450){
        btns.forEach(e => {
            e.classList.remove('active');
            e.nextElementSibling.classList.remove('active');
            e.nextElementSibling.style.maxHeight = null;
        });
    }

})

